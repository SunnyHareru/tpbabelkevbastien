/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        this.receivedEvent('deviceready');
    },

    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

       listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

       console.log('Received Event: ' + id);
    },
};

// ------------------------------------------------
// 	jquery.dropdown.js	(depends on: jquery.hoverIntent.js)
// ------------------------------------------------
$.fn.dropdown = function(options) {
  var defaults = {};
  var opts = $.extend(defaults, options);
  // Apply class=hasSub on those items with children
  this.each(function() {
    $(this)
      .find("li")
      .each(function() {
        if ($(this).find("ul").length > 0) {
          $(this).addClass("hasSub");
        }
      });
  });
  return this;
};
// ------------------------------------------------
// MENU MAIN
$(function() {
  var navMainId = "#navMain";
  // -------------------
  // Calling the jquery dropdown
  $(navMainId).dropdown();
  // -------------------
  // MENU ACTIF (class active) : Sous-Menu ouvert par defaut (FACULTATIF)
  /*
  $(navMainId + " ul > li.active").addClass("open");
  $(navMainId + " ul > li.active > ul").slideDown("fast");
  */
  // -------------------
  // ouverture/fermeture sous-menu (click/touch)
  $(navMainId + " ul > li").on("click", function(event) {
    event.stopPropagation(); /* important */
    $(this)
      .parent()
      .find("li:not(:hover)")
      .removeClass("open");
    $(this).toggleClass("open");
    $(this)
      .parent()
      .find("li:not(:hover) ul")
      .slideUp("fast");
    if ($(this).hasClass("hasSub")) {
      $(this)
        .children("ul")
        .slideToggle("fast");
    }
  });
  // -------------------
  // on désactive les liens des Menus AVEC Sous-Menus (obligatoire pour Tablettes TACTILES / Smartphones)
  $(navMainId + " > ul > li.hasSub > a").on("click", function(event) {
    event.preventDefault();
  });
  // -------------------
});

// ------------------------------------------------
// Scrollbar si menu plus grand que la hauteur de fenêtre
// (interessant pour MENU FIXE)
$(window).on("load resize", function() {
  var navMainId = "#navMain";
  //  A ADAPTER par rapport à la hauteur effectivement disponible : hors header, footer,....
//  $(navMainId).height( Math.min( $(window).height(), $(navMainId).height() ) );
  $(navMainId).height($(window).height());
  $(navMainId).css({ "overflow-y": "auto" });
});
// ------------------------------------------------


app.initialize();
