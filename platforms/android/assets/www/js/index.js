/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);

    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        this.receivedEvent('deviceready');
        checkConnection();
        document.getElementById("speechButton").addEventListener("click", onspeack);
    },

    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    }
};

app.initialize();

/* Le Lac est beau
Fraiche est son eau
C'est délicieeuux !

C'que nous voulons
C'est du poisson
Fort bien goûteeeuuux ! */


function checkConnection() {
    console.log("checkConnection");
    console.log(navigator.connection);
    var networkState = navigator.connection.type;

    var states = {};
    states[Connection.UNKNOWN]  = 'Unknown connection';
    states[Connection.ETHERNET] = 'Ethernet connection';
    states[Connection.WIFI]     = 'WiFi connection';
    states[Connection.CELL_2G]  = 'Cell 2G connection';
    states[Connection.CELL_3G]  = 'Cell 3G connection';
    states[Connection.CELL_4G]  = 'Cell 4G connection';
    states[Connection.CELL]     = 'Cell generic connection';
    states[Connection.NONE]     = 'No network connection';

    alert('Connection type: ' + states[networkState]);
}



function onspeack() {
    alert("3")
    onConversionSucess();
}

function onConversionSucess(){
  var requestCode = 12345;
  var maxMatches = 10;               // you can increase the number.
  var promptString = "Speak Now";    // you can change the prompt message.

        speechrecognizer.startListening(onSuccess, onError, requestCode, maxMatches, promptString);
}
 

function onConversionFail(){
    navigator.notification.alert("Could not convert the speech to text:" + error);
}

function onSuccess(data) {
if (data) {
var jsonObject = JSON.parse(data);
     // It will return the first matched text for the speech.
     var convertedText = jsonObject.speechMatches.speechMatch[0];
        $("#results").html(convertedText);    
}
}

function onError(error) {
navigator.notification.alert("Conversion failed due to: " + error);
}